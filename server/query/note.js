import mongoose from 'mongoose';
import { GraphQLString, GraphQLList } from "graphql";
import { NoteType } from "../type/note";

const NoteModel = mongoose.model('Note');

export const notes = {
    type: GraphQLList(NoteType),
    args: {},
    async resolve() {
        return NoteModel.find()
    }
};

export const note = {
    type: NoteType,
    args: {
        id: { type: GraphQLString, required: true },
    },
    async resolve(root, args) {
        return NoteModel.findById(args.id);
    }
};
