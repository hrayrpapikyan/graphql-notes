import mongoose from 'mongoose';
import {
    GraphQLBoolean,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql';
import { pubsub } from '../index'

const NoteModel = mongoose.model('Note');

const NoteMutation = new GraphQLObjectType({
    name: `NoteMutation`,
    fields: () => ({
        error: { type: GraphQLBoolean },
        message: { type: GraphQLString }
    })
});

export const createNote = {
    type: NoteMutation,
    args: {
        title: { type: GraphQLString },
        content: { type: GraphQLString }
    },
    async resolve(parentValue, args) {
        if (!args.title) {
            return {error: true, message: 'Please set title!'};
        }

        const newNote = await NoteModel.create({
            title: args.title,
            content: args.content,
        });
        newNote.save();
        pubsub.publish('note', { note: await NoteModel.findById(newNote.id)});
        return {error: false};
    }
};

export const updateNote = {
    type: NoteMutation,
    args: {
        note_id: { type: GraphQLString },
        title: { type: GraphQLString },
        content: { type: GraphQLString },
    },
    async resolve(parentValue, args) {

        const note = await NoteModel.findById(args.note_id);
        if (!note) {
            return {error: true, message: 'Note not found!'};
        }

        note.title = args.title;
        note.content = args.content;

        await note.save();

        pubsub.publish('note', { note });

        return {error: false};
    }
};

export const deleteNote = {
    type: NoteMutation,
    args: {
        note_id: { type: GraphQLString },
    },
    async resolve(parentValue, args) {
        const note = await NoteModel.findById(args.note_id);
        await note.remove();
        //don' need to publish here, it's just for demonstrating on graphiql
        return {error: false};
    }
};
