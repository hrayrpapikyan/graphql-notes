import mongoose from 'mongoose';
import {GraphQLBoolean, GraphQLObjectType, GraphQLString} from 'graphql';
import { pubsub } from '../index'

const NoteModel = mongoose.model('Note');

const CommentMutation = new GraphQLObjectType({
    name: `CommentMutation`,
    fields: () => ({
        error: { type: GraphQLBoolean },
        message: { type: GraphQLString }
    })
});

export const addComment = {
    type: CommentMutation,
    args: {
        content: {type: GraphQLString},
        note_id: {type: GraphQLString},
    },
    async resolve(parentValue, args) {

        const note = await NoteModel.findById(args.note_id);
        if (!note) {
            return {error: true, message: 'Note was not found!'};
        }
        note.comments.push({ content: args.content });
        pubsub.publish('note', { note: await note.save()});

        return {error: false};

    }
};

