// First of all importing and registering models
import './models';
import {
    createServer,
    Server,
} from 'http';
import {
    SubscriptionServer
} from 'subscriptions-transport-ws';
import {
    execute,
    subscribe,
} from 'graphql';
import GraphHTTP from 'express-graphql';
import express from 'express';
import cors from 'cors';
import { PubSub } from 'graphql-subscriptions';

export const pubsub = new PubSub();

// Building schema types and keeping inside index
import schema from './schema_builder';

/** BASE Express server definition **/
const app = express();

// main endpoint for GraphQL Express
app.use('/graphiql', cors(), GraphHTTP({
    schema,
    graphiql: true,
}));

// Making plain HTTP server for Websocket usage
const server = Server(app);

/** GraphQL Websocket definition **/
SubscriptionServer.create({
    schema,
    execute,
    subscribe,
    onConnect: () => console.log('ws connected')
}, {
    server: server,
    path: '/api/ws',
}, );


server.listen('PORT' in process.env ? process.env['PORT'] : 4000);

