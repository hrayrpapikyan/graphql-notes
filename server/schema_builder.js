import { GraphQLSchema, GraphQLObjectType } from 'graphql';
import {notes, note} from './query/note';
import { createNote, deleteNote, updateNote } from "./mutation/note";
import { addComment } from "./mutation/comment";
import { noteSubscription } from './subscription/note';

/**
 * Schema Building for user and repository;
 * @type {GraphQLSchema}
 */

export default new GraphQLSchema ({
  query: new GraphQLObjectType ({
    name: 'Query',
    fields: {notes, note}
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: {
      createNote,
      deleteNote,
      updateNote,
      addComment
    }
  }),
  subscription: new GraphQLObjectType({
    name: 'subscription',
    fields: {note : noteSubscription}
  })
});
