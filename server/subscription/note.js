import { pubsub } from '../index'
import { NoteType } from '../type/note'

export const noteSubscription = {
    type: NoteType,
    args: {},
    subscribe: () => pubsub.asyncIterator('note'),
};
