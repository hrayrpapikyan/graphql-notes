import mongoose from 'mongoose';
import { noteSchema } from "./note";

mongoose.set('useCreateIndex', true);
mongoose.pluralize(null);
mongoose.Promise = global.Promise;

mongoose.connect(('mongodb://127.0.0.1:27017/notes'), {useNewUrlParser: true},)
.then(() => console.log('Db Connected'));

mongoose.connection.on('error',  error => {
    console.log('Db not connected', {error});
});

mongoose.model('Note', noteSchema);
