import { Schema } from 'mongoose';

const commentSchema = Schema({
    content: String,
    created_at: { type: Date, default: Date.now },
});

export const noteSchema = Schema({
    title: String,
    content: String,
    comments: [commentSchema]
});
