import { GraphQLObjectType, GraphQLString } from "graphql";

export const CommentType = new GraphQLObjectType({
    name: 'CommentType',
    fields: {
        content: { type: GraphQLString, required: true },
        created_at: { type: GraphQLString }
    }
});