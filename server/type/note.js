import { GraphQLObjectType, GraphQLString, GraphQLList } from "graphql";
import {CommentType} from "./comment";

export const NoteType = new GraphQLObjectType({
    name: 'NoteType',
    fields: {
        title: { type: GraphQLString, required: true },
        content: { type: GraphQLString },
        id: { type: GraphQLString },
        comments: { type: GraphQLList(CommentType) }
    }
});

