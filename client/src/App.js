import React from 'react';
import _ from "lodash";
import { graphql } from 'react-apollo';
import './App.css';
import ListContainer from "./components/ListContainer";
import { GET_NOTES, NOTE_SUBSCRIPTION } from "./graphql-data-access";

const App = ({notes: {subscribeToMore, error, notes }}) => {
    subscribeToMore({
        document: NOTE_SUBSCRIPTION,
        variables: {},
        updateQuery: (prev, {subscriptionData}) => {
            if (!subscriptionData.data) {
                return prev;
            }
            const { notes } = prev;
            const newNote = subscriptionData.data.note;

            const newNoteExistsInPreviousNotes = !!_.find(notes, note => note.id === newNote.id);
            return newNoteExistsInPreviousNotes ?
                {
                    ...prev,
                    notes: _.map(prev.notes, note => note.id === newNote.id ? newNote : note)
                } :
                {
                    ...prev,
                    notes: [
                        newNote,
                        ...prev.notes,
                    ]
                }
        }
    });
    if (error) return <div>{error}</div>;
    return (
        <div className="main-wrapper">
            <ListContainer notes={notes || []}/>
        </div>
    );
};

export default graphql(GET_NOTES, {name: 'notes'})(App);
