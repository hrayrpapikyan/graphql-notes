import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import { SubscriptionClient } from 'subscriptions-transport-ws';

const WS_HOST = 'ws://localhost:4000';
export const WSClient = new SubscriptionClient(`${WS_HOST}/api/ws`, {
    reconnect: true,
});

WSClient.onError(error => console.log(error));
WSClient.onDisconnected(() => console.log('Websocket disconnected'));

const ErrorHandler = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.forEach(({ message}) =>
            console.log(`[GraphQL error]: ${message}`),
        );
    if (networkError) {
        console.log(`[Network error]: ${networkError}`);
    }
});

export const GraphQLClient = new ApolloClient({
    link: ApolloLink.from([ErrorHandler, WSClient]),
    cache: new InMemoryCache().restore(window.__APOLLO_STATE__ || {}),
});



