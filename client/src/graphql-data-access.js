import gql from "graphql-tag";

export const CREATE_NOTE = gql`
  mutation CreateNote($title: String, $content: String) {
    createNote(title: $title, content: $content) {
    error
    message
    }
  }
`;

export const UPDATE_NOTE = gql`
  mutation UpdateNote($id: String, $title: String, $content: String) {
    updateNote(note_id: $id, title: $title, content: $content) {
    error
    message
    }
  }
`;

export const ADD_COMMENT = gql`
  mutation AddComment($content: String, $note_id: String) {
    addComment(content: $content, note_id: $note_id) {
    error
    message
    }
  }
`;

export const NOTE_SUBSCRIPTION = gql`
  subscription Note {
    note {
        id
        title
        content
        comments {
            content
            created_at
        }
    }
  }
`;

export const GET_NOTES = gql`
  {
    notes {
        id
        title
        content
        comments {
            content
            created_at
        }
    }
  }
`;

export const GET_NOTE = gql`
    query getNote($id:String!) {
        note(id:$id) {
            id
        }
    }
`;