import React, {useState, useEffect} from 'react';
import { graphql } from "react-apollo";
import { compose } from 'ramda';
import moment from 'moment';
import { ADD_COMMENT, UPDATE_NOTE } from "../graphql-data-access";

let contentTimeout;
let titleTimeout;

const NoteDetails = ({ note, updateNoteMutation, addCommentMutation }) => {
    const [content, setContent] = useState(note.content || '');
    const [title, setTitle] = useState(note.title || '');
    const [comment, setComment] = useState( '');

    useEffect(() => {
        setContent(note.content || '')
    }, [note.content]);

    useEffect(() => {
        setTitle(note.title || '')
    }, [note.title]);

    useEffect(() => {
        setTitle(note.title || '')
        setContent(note.content || '')
        setComment('')
    }, [note.id]);


    useEffect(() => {
        clearTimeout(contentTimeout)
        contentTimeout = setTimeout(() => {
            if (content !== note.content && !(content === '' && !note.content)) {
                updateNoteMutation({
                    variables: {
                        id: note.id,
                        title: note.title,
                        content,
                    }
                })
            }

        }, 1000)

    }, [content]);

    useEffect(() => {
        clearTimeout(titleTimeout);
        titleTimeout = setTimeout(() => {
            if (title !== note.title) {
                updateNoteMutation({
                    variables: {
                        id: note.id,
                        title,
                        content: note.content,
                    }
                })
            }

        }, 1000)

    }, [title]);

    const onChangeContent = evt => {
        setContent(evt.target.value);
    };

    const onChangeTitle = evt => {
        setTitle(evt.target.value);
    };

    const addComment = async () => {
        await addCommentMutation({
            variables: {
                note_id: note.id,
                content: comment,
            }
        });
        setComment('');
    };

    return (
        <div className="note-bottom">
            <input className="note-field" placeholder="Note title" value={title} onChange={onChangeTitle}/>
            <textarea className="comment-textarea note-field"
                placeholder="Note content"
                value={content}
                onChange={onChangeContent}
            />

            <div className="comments-block">
                {note.comments.map((comment, id) => (
                    <div className="comment-wrapper" key={id}>
                        <span className="comment-txt" system={comment.system}  dangerouslySetInnerHTML={{ __html: comment.content.replace(/\n/g, '<br>')}} />
                        <span className="comment-date">{moment(comment.created_at).fromNow()}</span>
                    </div>
                ) )}
            </div>
            <textarea className="comment-textarea note-field comment-field"
                placeholder="Write a comment"
                value={comment}
                onChange={evt => { setComment(evt.target.value)}}
            />
            <button className="note-btn" onClick={addComment} disabled={!comment}>Add Comment</button>

        </div>
    );
};


export default compose(
    graphql(UPDATE_NOTE, {name: 'updateNoteMutation'}),
    graphql(ADD_COMMENT, {name: 'addCommentMutation'}),
)(NoteDetails);
