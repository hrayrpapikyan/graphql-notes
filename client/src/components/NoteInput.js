import React, {useState} from 'react';
export default ({onCreate}) => {
    const [value, setValue] = useState('');
    const onKeyPress = (evt) => {
        if(evt.key === 'Enter') {
            onCreate(value);
            setValue('');
        }
    };
    return (<div className="note-block">
        <div className="note-field-out">
            <input
                className="note-field main-field"
                placeholder="Write a title and press enter to add a note"
                onKeyPress={onKeyPress}
                value={value}
                onChange={evt => { setValue(evt.target.value)}}
            />
        </div>
    </div>);

}