import React from 'react';

export default ({title, onClick, isSelected}) => {
    return (<li className="note-list__item">
        <div className={`note-list__title ${isSelected && 'note-list__title_active'}`} onClick={onClick}>
            <span>{title}</span>
        </div>
    </li>);
}