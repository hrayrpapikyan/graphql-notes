import React, { useState } from 'react';
import _ from 'lodash';
import List from './List';
import NoteDetails from './NoteDetails';

const ListContainer  = ({notes}) => {
    const [selectedNoteId, setSelectedNoteId] = useState(null);
    return (
        <div className="inner-wrapper">
            <List onNoteSelect={noteId => setSelectedNoteId(noteId)} notes={notes} selectedNoteId={selectedNoteId}/>
            {!!selectedNoteId && (
                <NoteDetails note={_.find(notes, note => note.id === selectedNoteId)}/>
            )}
        </div>
    );
};

export default ListContainer;