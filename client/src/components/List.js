import React from 'react';
import {graphql} from 'react-apollo';
import _ from 'lodash';
import { CREATE_NOTE } from '../graphql-data-access';
import Note from './Note'
import NoteInput from './NoteInput'

const List = ({notes, selectedNoteId, createNoteMutation, onNoteSelect}) => {

    const onCreate = async title => createNoteMutation({variables: { title }});

    return (
        <div className="note-top">
            <NoteInput onCreate={onCreate}/>
            {!_.isEmpty(notes) && (
                <ul className="note-list">
                    {notes.map(note => (
                        <Note
                            {...note}
                            key={note.id}
                            onClick={_.partial(onNoteSelect, note.id)}
                            isSelected={note.id === selectedNoteId}
                        />)
                    )}
                </ul>
            )}
        </div>
    )
};

export default  graphql(CREATE_NOTE, {name: 'createNoteMutation'})(List);
