#GraphQL demo app

You can create and edit notes on a realtime dashboard

## Server

###Prerequisites

node >= v8.11.1
npm
mongoDB
mongod

###install dependencies

`cd server`
`npm i`

### start server

`mongod` to start MongoDb server
`cd server`
`npm start` will run the server on localhost:4000 with nodemon

### graphiql

navigate your browser to `http://localhost:4000/graphiql`

## Client

###install dependencies

`cd client`
`npm i`

###start server

`cd client`
`npm start`